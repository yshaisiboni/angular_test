// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDN28-dccQnyIHGQA7Ke6uEes09_m5W3bc",
    authDomain: "test-ishay.firebaseapp.com",
    projectId: "test-ishay",
    storageBucket: "test-ishay.appspot.com",
    messagingSenderId: "848504100580",
    appId: "1:848504100580:web:75b928a4c0b5bdfe377ef1"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
