import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Customer } from './interfaces/customer';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  customersCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');
  
  public getCustomers(userId): Observable<any[]> {
    this.customersCollection = this.db.collection(`users/${userId}/customers`);
    return this.customersCollection.snapshotChanges();
  }

  updateCustomer(userId:string, id:string, name:string, years:number, income:number){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        name:name,
        years:years,
        income:income,
        result:null
      }
    )
  }

  deleteCustomer(userId:string , id:string){
    this.db.doc(`users/${userId}/customers/${id}`).delete();
  }

  addCustomer(userId:string, name:string, years:number, income:number){
    const customer:Customer = {name:name, years:years, income:income};
    this.userCollection.doc(userId).collection(`customers`).add(customer);

  }

  updateResult(userId:string, id:string, result:string){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        result:result
      })
    }



  constructor(private db:AngularFirestore) { }
}
