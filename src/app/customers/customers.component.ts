import { Customer } from './../interfaces/customer';
import { PredictionService } from './../prediction.service';
import { CustomersService } from './../customers.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  customers:Customer[];
  customers$;
  userId:string;
  rowToEdit:number = -1; 
  customerToEdit:Customer = {name:null, years:null, income:null};
  addCustomerFormOpen = false;

  displayedColumns: string[] = ['name', 'Education in years', 'Personal income', 'Edit', 'Delete', 'Predict', 'Result'];

  moveToEditState(index){
    console.log(this.customers[index].name);
    this.customerToEdit.name = this.customers[index].name;
    this.customerToEdit.years = this.customers[index].years;
    this.customerToEdit.income = this.customers[index].income;
    this.rowToEdit = index; 
  }

  updateCustomer(){
    let id = this.customers[this.rowToEdit].id;
    this.CustomersService.updateCustomer(this.userId,id, this.customerToEdit.name,this.customerToEdit.years,this.customerToEdit.income);
    this.rowToEdit = null;
  }

  deleteCustomer(index){
    let id = this.customers[index].id;
    this.CustomersService.deleteCustomer(this.userId, id);
  }

  addCustomer(customer: Customer) {
    this.CustomersService.addCustomer(this.userId, customer.name, customer.years, customer.income);
  }

  updateResult(index){
    this.customers[index].saved = true; 
    this.CustomersService.updateResult(this.userId,this.customers[index].id,this.customers[index].result);
  }

  predict(index){
    this.predictionService.predict(this.customers[index].years, this.customers[index].income).subscribe(
      res => {console.log(res);
        if(res > 0.5){
          var result = 'Will pay';
        } else {
          var result = 'Will default'
        }
        this.customers[index].result = result}
    );  
  }

  constructor(private CustomersService: CustomersService, public authService: AuthService,
     public predictionService:PredictionService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
        this.customers$ = this.CustomersService.getCustomers(this.userId);
        this.customers$.subscribe(
          docs => {
            console.log(docs);
            this.customers = [];
              var i = 0;
              for (let document of docs) {
                console.log(i++); 
                const customer:Customer = document.payload.doc.data();
                if(customer.result){
                  customer.saved = true; 
                }
                customer.id = document.payload.doc.id;
                   this.customers.push(customer); 
              }                        
            }
        )
      }
    )

  }

}
