import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Customer } from '../interfaces/customer';

@Component({
  selector: 'customerform',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {

  @Input() name: string;
  @Input() years: number;
  @Input() income: number;
  @Input() id: string;

  @Output() update = new EventEmitter<Customer>();
  @Output() closeEdit = new EventEmitter<null>();

  tellParentToClose(){
    this.closeEdit.emit();
  }
  
  updateParent(){
    let customer:Customer = {id:this.id,name:this.name, years:this.years,income:this.income}; 
    this.update.emit(customer);
  }

  constructor() { }

  ngOnInit(): void {
  }

}
