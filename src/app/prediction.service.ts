import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictionService {
  private url = "https://d422qn7d78.execute-api.us-east-1.amazonaws.com/beta-example";

  predict(years:number, income:number):Observable<any> {
    let json = {
      "data": {
        "years": years,
        "income": income
      }
    }
    

    let body = JSON.stringify(json); //text to input in internet
    console.log("in predict");
    return this.http.post<any>(this.url, body).pipe(
      map(res => {
        console.log(res);
        console.log("in the map"); 
        console.log(res);
        console.log(res.body);
        return res.body;    
      })
    )
  }

  constructor(private http:HttpClient) { }
}
